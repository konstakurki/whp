import requests
import sys
import colored
#import whp

highlight1 = colored.bg('black') + colored.fg('red')
highlight2 = colored.bg('black') + colored.fg('green')
reset = colored.attr('reset')


def fetch(dicttype,searchword):
    url = 'http://en.bab.la/dictionary/'
    url = url + dicttype + '/' + searchword
    resp = requests.get(url)
    return resp.content

def boldtohighlight(string):
    string = string.replace('<b>',highlight1)
    string = string.replace('</b>',reset)
    return string

def result(data,n):
    n1 = data.find('<div class="span6 result-left"><p>',n)
    n2 = data.find('</p>',n1)
    if n1 != -1:
        sent1 = boldtohighlight(data[n1+34:n2])
        n1 = data.find('<span class="span9">',n2)
        n2 = data.find('</span>',n1)
        sent2 = highlight2 + data[n1+20:n2] + reset
    else:
        sent1 = ''
        sent2 = ''
    
    return [n1] + [sent1+ " " + sent2]

def parse(data):
    results = []
    n = data.find('<h2 class="babH2Gr">Context sentences')
    rslt = result(data,n)
    n = rslt[0]
    while n != -1:
        results = results + [rslt[1]]
        #print(rslt[1])
        rslt = result(data,n)
        n = rslt[0]
    #print(results)

    return results
    

def main():
    dicttype = 'finnish-english'
    searchword = sys.argv[1]
    data = fetch(dicttype,searchword)
    #File = open('bablamockupdata.txt')
    #data = File.read()
    #File.close()

    for x in parse(data):
        print('')
        print(x)

if __name__ == '__main__':
    main()
