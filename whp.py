import sys
import colored
import os

terminalwidth = int(os.popen('stty size','r').read().split()[1])
reset = colored.attr('reset')
spacing = [" ","\n","\t"]
ending = [".","?","!",'."',">","\n"]



def sentBegins(data,n):
    i = n
    T = 1
    while T:
        #print(i)
        if isbeginning(data[i-7:i]) == 1:
            T = 0
        i = i - 1
    return i



def sentEnds(data,n):
    #l = len(data)
    nd = -3
    while nd == -3:
        nd = Ending(data[n:n+7])
        #print(n)
        n = n + 1
    return n + nd



def isbeginning(string):
    l = len(string)
    T = 0
    if string[-1].islower != 1:
        if string[-2] in spacing:
            n = -3
            while string[n] in spacing and l + n > 0:
                n = n - 1
            if string[n] in ending or string[n-1:n+1] in ending or n + l == 0:
                T = 1
    return T



def Ending(string):
    l = len(string)
    n = -1
    if string[0] in ending and string[1] in spacing:
        n = 2
    if string[0:2] in ending and string[2] in spacing:
        n = 3
    m = n
    n = 1
    if n != -1:
        while string[n] in spacing and n < l - 1:
            n = n + 1
        if string[n].islower() != 1 or n == l:
            n = m
        else: n = -1
    return n - 2



def sentence(data,n):
    maxsentlen = 400
    a = max(sentBegins(data,n),n-maxsentlen/2)
    b = min(sentEnds(data,n),n+maxsentlen/2)
    Sent = data[a:b]
    return Sent



def wrap(sent,searchword,highlight):
    sent = sent.replace("\n"," ")
    lines = []
    n = sent.rfind(" ",0,terminalwidth + 1)

    while len(sent) > terminalwidth:
        if n == -1:
            n = terminalwidth
            lines = lines + ["\n" + sent[0:n]]
            sent = sent[n:]
        else:
            lines = lines + ["\n" + sent[0:n]]
            sent = sent[n+1:]
        n = sent.rfind(" ",0,terminalwidth + 1)

    lines = lines + ["\n" + sent]
    lines[0] = lines[0][1:]

    # highlight
    for x in range(len(lines)):
        line = lines[x]
        m = line.lower().find(searchword)
        if m != -1:
            lines[x] = line[0:m]+highlight+line[m:m+len(searchword)]+reset+line[m+len(searchword):]
    sent = "".join(lines)
    return sent



def sentences(data,searchword,highlight):
    lcdata = data.lower()
    wordlength = len(searchword)
    datalength = len(data)

    locations = []
    n = lcdata.find(searchword)
    while n != -1:
        locations.append(n)
        n = n + wordlength
        n = lcdata.find(searchword,n)
    sents = [sentence(data,n) for n in locations]

    # Remove duplicates & wrap lines
    x = ""
    X = []
    for y in sents:
        if y != x:
            X.append(wrap(y,searchword,highlight))
        x = y
    return X



def resultlist(x,searchword):
    filename = x[0:x.find(' ')]
    color = x[40:x.find(' ',40)]
    highlight = colored.bg('black') + colored.fg(color)
    author = highlight + x[52:-1] + reset

    File = open(filename)
    data = File.read()
    File.close()

    results = sentences(data,searchword,highlight)
    wc = len(data)
    hits = len(results)
    return [author] + [hits] + [(1e6*hits)/wc] + results

def combine(lists):
    #lsts = []
    totlen = 0
    for x in lists:
        totlen = totlen + x[1]
        #lsts = lsts + x[3:]
    #print(totlen)
    #return lsts

    cl = []
    while len(cl) < totlen:
        for x in range(len(lists)):
            if len(lists[x]) > 3:
                cl = cl + [lists[x][-1]]
                lists[x].pop()
    return cl

#def combine(x):
    #return x[0]



def main():
    searchword = sys.argv[1]

    File =  open('whp.config')
    sourcepointers = File.readlines()
    File.close()

    results = [resultlist(x,searchword) for x in sourcepointers]

    for i in results:
        if x[1] != 0:
            print(x[0] + ' (' + str(x[1]) + ')')

    for i in combine(results):
        print('')
        print(i)

if __name__ == "__main__":
    main()
